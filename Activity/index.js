let number = Number(prompt("Enter a number: "));
console.log("The number you provided is " + number);

for (let num = number; num >= 0; num-=5) {
	

	if (num <= 50) {
		break;
	}

	if (num % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (num % 5 === 0) {
		console.log(num);
	}
}

let string1 = "supercalifragilisticexpialidocious";
console.log(string1);

let vowels = ['a', 'e', 'i', 'o', 'u'];

let consonants = '';

for (let x of string1) {
	if (vowels.indexOf(x) === -1) {
		consonants += x;
	}
}

console.log(consonants);

let con = '';
for (let x = 0; x < string1.length; x++) {
	if (string1[x].toLowerCase() !== "a" &&
		string1[x].toLowerCase() !== "e" &&
		string1[x].toLowerCase() !== "i" &&
		string1[x].toLowerCase() !== "o" &&
		string1[x].toLowerCase() !== "u" ) {
		con += string1[x];
	}
}
console.log(con)